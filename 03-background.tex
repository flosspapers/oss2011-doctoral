\section{Background}
\label{sec:background}

\subsection{Related projects}
\label{subsec:related-projects}

In this work so far, we have found some projects related to automatic
quality evaluation of free software:

\begin{itemize}
\item \textbf{FLOSSMetrics} (Free/Libre Open Source Software Metrics) is a project
that uses existing methodologies and tools to provide a large database
with information about free software development~\cite{FLOSSMetrics2008}.

\item \textbf{Ohloh} is a website that provides a web services suite and an on-line
community platform that aims at building an overview of free software
development~\cite{Ohloh2008}.

\item \textbf{Qualoss} (Quality in Open Source Software) is a methodology to
automate the quality measurement of free software projects, using tools to
analyze the source code and the project repository information~\cite{Qualoss2008}.

\item \textbf{SQO-OSS} (Software Quality Assessment of Open Source Software) provide a
suite of that allows analysis and benchmarking of free software
projects~\cite{SQO-OSS2008}.

\item \textbf{QSOS} (Qualification and Selection of Open Source Software) is a methodology
based on 4 steps: used reference definition; software evaluation; 
qualification of specific users context; selection and comparison of
software~\cite{QSOS2008}.

\item \textbf{FOSSology} (Advancing open source analysis and development) is a project
that provides a free database with information about the software
license~\cite{FOSSology2008}.

\item \textbf{QualiPSo} (Trust and Quality in Open Source Systems) defined procedures
to boost the use of free software and adoption of its development practices
within software industry~\cite{qualipso}. A set of tools was integrated
with QualiPSo Quality Platform.

\item \textbf{HackyStat} is an environment for analysis, visualization, interpretation
of software development process and product data~\cite{hackystat2011}.

\end{itemize}

In short, we have identified from current available projects and their tools
the lack of the following features:
%
(i) to collect automatically source code metrics values considering different
programming languages;  
(ii) to interpret measurement results, associating them with source code
quality.
%
Therefore, we are developing the Kalibro Metrics tool that can be configured to
show metric results in a friendly way, helping software engineers
to spot design flaws to refactor, project managers to control source code
quality, and software researchers to compare specific source code
characteristics across free software projects.


\subsection{Source code analysis tools}
\label{subsec:related-tools}

During our research and development activities, we studied or used 11
free software source code analysis tools:
%
Analizo~\cite{analizo},
CCCC~\cite{cccc},
Checkstyle~\cite{checkstyle},
CMetrics~\cite{cmetrics},
CPPX~\cite{hassan2005},
Cscope~\cite{cscope}
CTAGX~\cite{hassan2005},
LDX~\cite{hassan2005}
JaBUTi~\cite{jabuti},
MacXim~\cite{macxim}
and Metrics (Eclipse plug-in)~\cite{eclipsemetrics}.
%
Also, we have defined the following requirements for our tool, according to our
theoretical and practical needs, as well as our ideas to explorer better source
code metrics:

\begin{itemize}

  \item The tool should support source code metrics \textbf{thresholds} to
provide different interpretations about metric values.

  \item The tool should support the analysis of different programming languages
(\textbf{multi-language}).

  \item The tool should provide clear interfaces for adding new metrics and supporting
different programming language (\textbf{extensibility}).

  \item The tool should be \textbf{free software}, available without
restrictions to allow other researchers to replicate our studies and results
fully.

  \item The tool should be supported by active developers who know the tool
architecture (\textbf{maintained}).
 
\end{itemize}


\begin{table}[htb]
  \centering
%  \scalefont{0.95}
  \begin{tabular}{|l|c|c|c|c|}
    \hline
    \textbf{Tools} &
    \textbf{Languages} & %Multi-language
    \textbf{Extensible} & %Extensibility
    \textbf{Thresholds} & % To Support Thresholds
    \textbf{Maintained} \\\hline\hline %Actively maintained 
				    
    Analizo 	& C, C++, Java & Yes & No  & Yes    \\\hline
    CCCC 	& C++, Java    & No  & No  & Yes    \\\hline
    Checkstyle	& Java         & No  & Yes & Yes    \\\hline
    CMetrics	& C            & Yes & No  & Yes    \\\hline
    CPPX	& C, C++       & No  & No  & No     \\\hline
    Cscope	& C            & No  & No  & Yes    \\\hline
    CTAGX	& C            & No  & No  & No     \\\hline
    LDX		& C, C++       & No  & No  & No     \\\hline
    JaBUTi	& Java         & No  & No  & Yes    \\\hline
    MacXim	& Java         & No  & No  & Yes    \\\hline
    Metrics 	& Java         & No  & Yes & Yes    \\\hline
    
  \end{tabular}


  \caption{Existing tools versus our defined requirements}
  \label{tab:tools}
%  \scalefont{1}
\end{table}
\vspace{-1em}

In Table \ref{tab:tools}, we compare all of these tools to our requirements.
%
The Analizo~\cite{analizo} tool is indicated as able to analyze source code from our
initial three required languages (C, C++, and Java), at first moment,
it did not support the Java language.  
%
Because Analizo has documented extension interfaces, we could help its
software engineers to implement the support to Java and new metrics.

In addition, we want a tool with thresholds support, and Analizo metric tool
needed a graphical interface and a friendlier approach.
%
Thus, Analizo acts as the Kalibro default source code analysis tool since Kalibro
Metrics supports different source code collector tools.
%
As we have observed during the development of Java support for Analizo, the
multi-language approach may constrain the implementation of some metrics we 
need.
%
For example, there are specific metrics for Java which we had difficulties to
implement.
%
Thus, an interesting differential from Kalibro Metrics is the support for 
distinct source code analysis tools.

\subsection{Related works}

To select which metrics we should study their thresholds and relate them to
clean code concepts, we investigating which source code metrics influence in 
free software projects success, i.e., its attractiveness.
%
Santos Jr. \emph{et al.}~\cite{Santos2010} defined a theoretical model for
attractiveness as a crucial construct for free software projects, proposing their
(i) typical origins (e.g., license type and intended audience);
(ii) indicators (e.g., number of members and downloads);
(iii) consequences (e.g, levels of activity and time for task completion).
%
They suggested that the success of any project depends on
its level of attractiveness to potential contributors and users.
%
Based on this model, we are exploring some
of the factors that may enable projects to build a community by attracting users
and developers.

\begin{figure}[htpb]
\centering
\includegraphics[scale=.23]{figure/attractiveness3}
\caption{Attractiveness research model
-- adapted from Santos Jr. et al~\cite{Santos2010}.}
\label{attractiveness}
\end{figure}

In short, we have proposed a new element that can explain attractiveness partially.
%
According to our first hypotheses, we added source code attributes (from source code metrics)
and expect that they would work in the same causal chain manner as shown in
Figure~\ref{attractiveness}.
%
To test our ideas empirically, we analyzed 6,773 projects
written in the C language from SourceForge.net~\cite{meirelles2010}.
%
Our first study was able to explain 18\% of software download and 12\% of project
members, through a set of four source code metrics.
%
Currently, we are collecting data from 42.335 projects
written in the C, C++, and Java languages.

A systematic review of 63 empirical studies showed that there is little research
addressing the characteristics or properties of free software projects,
such as their quality, growth, and evolution \cite{Stol2009}.
%
In this context, we are analyzing source code metrics from 
an unprecedented number of free software projects, linking their source code
characteristics and attractiveness.

For example (comparing our samples to others from related works), Barkmann
\emph{et al.}~\cite{Barkmann2009} analyzed 146 free software projects written
in Java, identifying the correlation between a set of object-oriented
metrics and their theoretical ideal values. 
%
Stamelos \emph{et al.} \cite{Stamelos2002} compared quality characteristics of
100 applications written for GNU/Linux to industrial standards.
%
Midha \cite{midha2008} analyzed 450 projects from SourceForge.net and
verified that high values of MacCabe's Cyclomatic Complexity and
Haltead's Effort are positively correlated
with the number of bugs and with the time needed to fix bugs.
%
Capra \emph{et al.} \cite{capra2008} have shown that open governance is associated
with higher software design quality on a study with 75 free software projects.
%
Finally, Bargallo \emph{et al.} \cite{bargallo2008} analyzed 56 free software projects,
studying the relationship between software design quality and project success.

% ~ 1.350 words
